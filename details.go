package report

import (
	"encoding/json"
)

// Details contains properties which abide by the details attribute of the Secure Report Schemas:
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/src/vulnerability-details-format.json
type Details map[string]interface{}

// DetailsTextField stores a raw text detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/src/vulnerability-details-format.json#L123
type DetailsTextField struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// DetailsURLField stores a raw url detail type
// https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/src/vulnerability-details-format.json#L139
type DetailsURLField struct {
	Name string `json:"name"`
	Text string `json:"text"`
	Href string `json:"href"`
}

// MarshalJSON turns a DetailsTextField into a json object
func (d DetailsTextField) MarshalJSON() ([]byte, error) {
	type rawDetailsTextField DetailsTextField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsTextField
	}{
		"text",
		rawDetailsTextField(d),
	}

	return json.Marshal(typed)
}

// MarshalJSON turns a DetailsURLField into a json object
func (d DetailsURLField) MarshalJSON() ([]byte, error) {
	type rawDetailsURLField DetailsURLField
	var typed = struct {
		Type string `json:"type"`
		rawDetailsURLField
	}{
		"url",
		rawDetailsURLField(d),
	}

	return json.Marshal(typed)
}
