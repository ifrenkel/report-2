package report

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

var (
	scanner = Scanner{
		ID:   "AnalyzerID",
		Name: "AnalyzerName",
	}
)

func TestUnsupportedSarifVersion(t *testing.T) {
	minimalSarif := `
		{
			"version": "2.0.0"
		}
	`

	report := strings.NewReader(minimalSarif)
	_, err := TransformToGLSASTReport(report, "/tmp/app", "analyzerID", scanner)

	expected := fmt.Errorf("version for SARIF is 2.0.0, but we only support 2.1.0")

	require.NotNil(t, err)
	require.Equal(t, expected.Error(), err.Error())
}

func TestTransformToGLSASTReport(t *testing.T) {
	tests := []struct {
		name       string
		reportPath string
		wantReport string
		env        []string
		scanner    Scanner
	}{
		{
			name:       "semgrep sarif with security-severity",
			reportPath: "testdata/reports/semgrep_security_severity.sarif",
			wantReport: "testdata/expect/sarif.semgrep.security_severity.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", "sast_fp_reduction"},
			scanner:    Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep shortDescription and fullDescription are equal for title example (ensure no panic if CWE can't parse)",
			reportPath: "testdata/reports/semgrep_short_description_equal_full.sarif",
			wantReport: "testdata/expect/sarif.semgrep.short_description_equal_full.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", "sast_fp_reduction"},
			scanner:    Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep sarif shortDescription for title example",
			reportPath: "testdata/reports/semgrep_short_description.sarif",
			wantReport: "testdata/expect/sarif.semgrep.short_description.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", "sast_fp_reduction"},
			scanner:    Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "semgrep sarif example",
			reportPath: "testdata/reports/semgrep.sarif",
			wantReport: "testdata/expect/sarif.semgrep.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", "sast_fp_reduction"},
			scanner:    Scanner{ID: "semgrep", Name: "semgrep"},
		},
		{
			name:       "KICS sarif example",
			reportPath: "testdata/reports/kics.sarif",
			wantReport: "testdata/expect/sarif.kics.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", ""},
			scanner:    Scanner{ID: "kics", Name: "kics"},
		},
		{
			name:       "KICS sarif with 'unknown' severity",
			reportPath: "testdata/reports/kics_with_unknown_severity.sarif",
			wantReport: "testdata/expect/sarif.kics_with_unknown_severity.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", ""},
			scanner:    Scanner{ID: "kics", Name: "kics"},
		},
		{
			name:       "semgrep sarif excluding suppressions",
			reportPath: "testdata/reports/semgrep_with_suppressions.sarif",
			wantReport: "testdata/expect/sarif.semgrep.excl-suppr.gl-sast-report.json",
			env:        []string{"GITLAB_FEATURES", ""},
			scanner:    Scanner{ID: "semgrep", Name: "semgrep"},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			t.Setenv(tc.env[0], tc.env[1])

			report, err := os.Open(tc.reportPath)
			require.NoError(t, err)
			defer report.Close()

			got, err := TransformToGLSASTReport(report, "/tmp/app", "analyzerID", tc.scanner)
			require.NoError(t, err)

			// Analyzer and Config are not set in json
			got.Analyzer = ""
			got.Config = ruleset.Config{}
			// Remediations are nil as they were omitted from json report for being empty
			got.Remediations = nil

			var want Report
			wantReportBytes, err := ioutil.ReadFile(tc.wantReport)
			require.NoError(t, err)

			err = json.Unmarshal(wantReportBytes, &want)
			require.NoError(t, err)
			require.Equal(t, &want, got)
		})
	}
}

func TestToolExecutionNotifications(t *testing.T) {
	tests := []struct {
		path   string
		errMsg string
	}{
		{"testdata/reports/semgrep_invalid_config.sarif", "tool notification error: InvalidRuleSchemaError Additional properties are not allowed ('pattern-eitherrr' was unexpected)"},
		{"testdata/reports/semgrep_js_syntax_error.sarif", "Semgrep Core WARN - Syntax error: When running eslint.detect-non-literal-require"},
		{"testdata/reports/semgrep_nonmatching_nosem.sarif", "found 'nosem' comment with id 'bandit.B502'"},
	}

	for _, tc := range tests {
		t.Run(tc.path, func(t *testing.T) {
			fixture, err := os.Open(tc.path)
			require.NoError(t, err)

			logWriter := bytes.NewBuffer(nil)
			log.SetOutput(logWriter)

			_, err = TransformToGLSASTReport(fixture, "/tmp/app", "analyzerID", scanner)
			if err != nil {
				t.Error("expected nil error")
				return
			}

			logOut := logWriter.String()
			if !strings.Contains(logOut, tc.errMsg) {
				t.Errorf("expected: %v\n but got: %v", tc.errMsg, logOut)
			}
		})
	}
}

func TestRemoveRootPath(t *testing.T) {
	tests := []struct {
		path     string
		rootPath string
		expected string
	}{
		{"/a/b/c/d.foo", "/a/b/", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/b", "c/d.foo"},
		{"/a/b/c/d.foo", "/a/c", "/a/b/c/d.foo"},
		{"a/b/c/d.foo", "/a/b", "c/d.foo"},
	}

	for _, tc := range tests {
		got := removeRootPath(tc.path, tc.rootPath)
		require.Equal(t, tc.expected, got)
	}
}

func TestSeverityParsing(t *testing.T) {
	tests := []struct {
		severity string
		expected SeverityLevel
	}{
		{
			"unknown",
			SeverityLevelUnknown,
		},
		{
			"  UnKnOwN  ",
			SeverityLevelUnknown,
		},
		{
			"asdf",
			SeverityLevelCritical,
		},
		{
			"Critical",
			SeverityLevelCritical,
		},
		{
			"High",
			SeverityLevelHigh,
		},
	}

	for _, tc := range tests {
		r := rule{
			DefaultConfiguration: struct {
				Level string `json:"level"`
			}{
				Level: "error",
			},
			Properties: ruleProperties{
				SecuritySeverity: tc.severity,
			},
		}

		got := severity(r)
		require.Equal(t, tc.expected, got)
	}
}

func TestIdentifierRegexes(t *testing.T) {
	tests := []struct {
		identifier string
		expected   Identifier
	}{
		{"cwe-127", Identifier{Type: "cwe", Name: "CWE-127", Value: "127", URL: "https://cwe.mitre.org/data/definitions/127.html"}},
		{"CWE-127", Identifier{Type: "cwe", Name: "CWE-127", Value: "127", URL: "https://cwe.mitre.org/data/definitions/127.html"}},
		{"CWE-295: Improper Certificate Validation", Identifier{Type: "cwe", Name: "CWE-295", Value: "295", URL: "https://cwe.mitre.org/data/definitions/295.html"}},
		{"OWASP-A3: Sensitive Data Exposure", Identifier{Type: "owasp", Name: "A3 - Sensitive Data Exposure", Value: "A3", URL: ""}},
		{"TestTag-BRxZy: Some Description", Identifier{Type: "testtag", Name: "Some Description", Value: "BRxZy", URL: ""}},
		{"invalid tag", Identifier{}},
	}

	for _, tc := range tests {
		r := rule{
			DefaultConfiguration: struct {
				Level string `json:"level"`
			}{
				Level: "error",
			},
			Properties: ruleProperties{
				SecuritySeverity: "High",
				Tags:             []string{tc.identifier},
			},
		}
		ids := identifiers(r, scanner)
		if strings.EqualFold(tc.identifier, "invalid tag") {
			require.Len(t, ids, 1)
			continue
		}
		require.Len(t, ids, 2)
		require.Equal(t, tc.expected, ids[1])
	}
}

func TestIdentifiersFromTags(t *testing.T) {
	baseIdentifier := Identifier{
		Type:  "_id",
		Name:  "RuleName",
		Value: "ID",
	}
	OWASPGenericIdentifier := Identifier{
		Type:  "OWASP",
		Name:  "A3 - Sensitive Data Exposure",
		Value: "A3",
	}
	OWASPYearIdentifier := Identifier{
		Type:  "OWASP",
		Name:  "A03:2017 - Sensitive Data Exposure",
		Value: "A03:2017",
	}

	tests := []struct {
		name                string
		r                   rule
		s                   Scanner
		expectedIdentifiers []Identifier
	}{
		{
			name: "with no secondary identifiers",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
			},
		},
		{
			name: "with CWE identifier",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: ruleProperties{
					Tags: []string{
						"CWE-295: Improper Certificate Validation",
					},
				},
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
				CWEIdentifier(295),
			},
		},
		{
			name: "with OWASP generic identifier",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: ruleProperties{
					Tags: []string{
						"OWASP-A3: Sensitive Data Exposure",
					},
				},
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
				OWASPGenericIdentifier,
			},
		},
		{
			name: "with OWASP year identifier",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: ruleProperties{
					Tags: []string{
						"OWASP-A03:2017 - Sensitive Data Exposure",
					},
				},
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
				OWASPYearIdentifier,
			},
		},
		{
			name: "with OWASP strict identifier",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: ruleProperties{
					Tags: []string{
						"OWASP-A03:2017 - Sensitive Data Exposure",
					},
				},
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
				OWASPYearIdentifier,
			},
		},
		{
			name: "with multiple secondary identifiers",
			r: rule{
				ID:   "ID",
				Name: "RuleName",
				Properties: ruleProperties{
					Tags: []string{
						"CWE-295: Improper Certificate Validation",
						"OWASP-A3:Sensitive Data Exposure",
						"OWASP-A03:2017 - Sensitive Data Exposure",
						"OWASP-A3:NotYear - Tag",
						"OWASP:Skippable malformed Tag",
					},
				},
			},
			s: Scanner{},
			expectedIdentifiers: []Identifier{
				baseIdentifier,
				CWEIdentifier(295),
				OWASPGenericIdentifier,
				OWASPYearIdentifier,
				{
					Type:  "OWASP",
					Name:  "A3 - NotYear - Tag",
					Value: "A3",
				},
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			ids := identifiers(tc.r, tc.s)
			require.Len(t, ids, len(tc.expectedIdentifiers))
			for idx, id := range ids {
				require.Equal(t, tc.expectedIdentifiers[idx].Name, id.Name)
			}
		})
	}
}
